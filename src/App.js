import React, { useState } from "react";

const App = () => {
    const [details, setDetails] = useState(null);
    const getUserGeolocationDetails = () => {
        fetch(
            "https://geolocation-db.com/json/09068b10-55fe-11eb-8939-299a0c3ab5e5"
        )
            .then(response => response.json())
            .then(data => setDetails(data));
    };

    if(!details)
      getUserGeolocationDetails();

    // deatils.country_code return the CODE location of the visitor
      return (
          <>
            <div className="row">
                <div className="text-center">
                    {details && (details.country_code === "US" ?
                    <div>
                    <h3>Welcome to the Mexican Database of Heroku Users on the Universidad Marista at Guadalajara</h3>
                            <table>
                                <tr>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Age</th>
                                    <th>Company</th>
                                </tr>
                                <tr>
                                    <td>Roberto</td>
                                    <td>Villanueva</td>
                                    <td>24</td>
                                    <td>Sánchez y Martin </td>
                                </tr>
                                <tr>
                                    <td>Julián</td>
                                    <td>Zepeda</td>
                                    <td>22</td>
                                    <td>Toshiba GCS</td>
                                </tr>
                                <tr>
                                    <td>Andrés</td>
                                    <td>Chávez</td>
                                    <td>22</td>
                                    <td>Wizeline</td>
                                </tr>
                                <tr>
                                    <td>Aldo Mauricio</td>
                                    <td>Vargas</td>
                                    <td>22</td>
                                    <td>Persistent Systems | IBM</td>
                                </tr>
                                <tr>
                                    <td>Marco</td>
                                    <td>Lafarga</td>
                                    <td>22</td>
                                    <td>Cleazen</td>
                                </tr>
                                <tr>
                                    <td>Christian</td>
                                    <td>Muñoz</td>
                                    <td>22</td>
                                    <td>Toshiba GCS</td>
                                </tr>
                            </table>
                        </div>
                    : details.country_code === "MX" ?
                    <div>
                        <h3>Disfruta de esta hermosa canción... </h3>
                        <br/>
                        <iframe title="Stereo Love" width="560" height="315" src="https://www.youtube.com/embed/p-Z3YrHJ1sU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    :
                    <div>
                        <h1>The content is not available outside MX or USA, lets learn about the usage of the VPNs</h1>
                        <iframe title="VPN" width="560" height="315" src="https://www.youtube.com/embed/aoJJIN49ECo?start=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    )}
                </div>
            </div>
        </>
      );
};

export default App;
